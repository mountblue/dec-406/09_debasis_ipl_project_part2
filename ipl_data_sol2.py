"""Calcultes the number of wins per team in
Each season
Data(IPL MATCHES DATA) : kaggle.com

Created at : 2019
Author : Debasis
"""

import config

query = ("select season,winner, count(*) from matches group by season,winner;")
print(config.excute_query(query))