"""  Connecting to the mysql database """
import mysql.connector

def excute_query(query):

    """Connects to the mysql database 
    Takes a query as a parameter 
    And returns a list of datas
    """
    cnx = mysql.connector.connect(user='root',password='password', database='matches')
    cursor = cnx.cursor(buffered=True)
    query = (query)
    cursor.execute(query)
    data = cursor.fetchall()
    cursor.close()
    cnx.close()
    return data
    