"""Calcultes extra runs given by each team
For season 2016

Data(IPL MATCHES DATA) : kaggle.com

Created at : 2019
Author : Debasis
"""

import config

query = ("""select matches.season, DELIVERIES.bowling_team, sum(DELIVERIES.extra_runs) 
        from matches.DELIVERIES inner join matches on DELIVERIES.match_id = matches.id
        where matches.season = 2016 group by DELIVERIES.bowling_team;""")
print(config.excute_query(query))