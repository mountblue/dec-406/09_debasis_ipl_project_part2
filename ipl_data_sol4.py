"""Calcultes top ten economical bowler 
For season 2015
Data(IPL MATCHES DATA) : kaggle.com

Created at : 2019
Author : Debasis
"""


import config

query = ("""select matches.season, DELIVERIES.bowler, 
        (sum(DELIVERIES.total_runs)/(count(DELIVERIES.total_runs)/6)) as Economy
        from matches.DELIVERIES inner join matches
        on DELIVERIES.match_id = matches.id where matches.season = 2015
        group by DELIVERIES.bowler order by Economy limit 10;""")
print(config.excute_query(query))